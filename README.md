# LFortran Logo

Logo of LFortran, in SVG and PNG formats.

## GitHub

Dimensions: 500px x 500px  
Filename: [lfortran_logo_white_bg_500x500.png](lfortran_logo_white_bg_500x500.png)

## Twitter

Dimensions: 400px x 400px

## Favicon

Dimensions: 32px x 32px
